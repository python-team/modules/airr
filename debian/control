Source: python-airr
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Steffen Moeller <moeller@debian.org>
Section: science
Testsuite: autopkgtest-pkg-python
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-sequence-python3,
               python3-all,
               python3-setuptools,
               python3-pandas,
               python3-yaml,
               python3-yamlordereddictloader
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/python-team/packages/airr
Vcs-Git: https://salsa.debian.org/python-team/packages/airr.git
Homepage: https://docs.airr-community.org/en/latest/packages/airr-python/overview.html
Rules-Requires-Root: no

Package: python3-airr
Architecture: all
Section: python
Depends: ${python3:Depends},
         ${misc:Depends}
Description: Data Representation Standard library for antibody and TCR sequences
 This package provides a library by the AIRR community to for describing,
 reporting, storing, and sharing adaptive immune receptor repertoire
 (AIRR) data, such as sequences of antibodies and T cell receptors
 (TCRs). Some specific efforts include:
  * The MiAIRR standard for describing minimal information about AIRR
    datasets, including sample collection and data processing information.
  * Data representations (file format) specifications for storing large
    amounts of annotated AIRR data.
  * APIs for exposing a common interface to repositories/databases
    containing AIRR data.
  * A community standard for software tools which will allow conforming
    tools to gain community recognition.
 .
 This package installs the library for Python 3.
